import {Form, Button, Row, Col, Container} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function Login() {
	//Initializes the use of properties from the UserProvider in App.js file 
	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')


	//Initialize useNavigate
	const navigate = useNavigate()

	const [isActive, setIsActive] = useState(false)

//works as a function es6 update
	const retrieveUser = (token) => {
			fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
					headers: {
						Authorization:`Bearer ${token}`
					}
			})
			.then(response => response.json())
			.then(result => {
				console.log(result)


				//Store the user details retrieved from the token in the global user state
					setUser({
						id: result._id,
						isAdmin:result.isAdmin
					})
			})
	}
//ending retrieveUser function

function authenticate(event){

	event.preventDefault()

	fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})	
	})
	.then(response => response.json())
	.then(result => {
			if(typeof result.accessToken !== "undefined"){
				localStorage.setItem('token', result.accessToken)
				// localStorage.getItem(token)

				retrieveUser(result.accessToken) 

				Swal.fire({
					title:'Login Successful!',
					icon: 'success',
					text: ''
				})
			}else{
				Swal.fire({
					title:'Authentication Failed!',
					icon: 'error',
					text: 'aGuy!'
				})
			}
	})
}

useEffect (() => {
	if (email !== '' && password !== ''){
    	setIsActive(true)	
    }else {
    	setIsActive(false)
    }
}, [email, password])

  return (
    	
    		(user.id !== null) ?
    		<Navigate to="/products"/>
    		:

    		<Container>
    			<Row>
        <Col xs={6}></Col>
        <Col xs={12} lg={6} md={6} className="mt-5">
        	<Form onSubmit = {event => authenticate(event)}>
    		     
    		      <Form.Group className="mb-3" controlId="formBasicEmail">
    		        <Form.Label>Email address</Form.Label>
    		        <Form.Control 
    		        type="email" 
    		        placeholder="Enter email"
    		        value={email}
    				onChange={event => setEmail(event.target.value)}
    				required
    				 />
    		        <Form.Text className="text-muted">
    		          We'll never share your email with anyone else.
    		        </Form.Text>
    		      </Form.Group>

    		      <Form.Group className="mb-3" controlId="formBasicPassword">
    		        <Form.Label>Password</Form.Label>
    		        <Form.Control 
    		        type="password" 
    		        placeholder="Password"
    		        value={password}
    				onChange={event => setPassword(event.target.value)}
    				required />
    		      </Form.Group>
    		      <Form.Group className="mb-3" controlId="formBasicCheckbox">
    		        <Form.Check type="checkbox" label="Agree to user terms" />
    		      </Form.Group>
    		      {	isActive ? 
    			        <Button variant="primary" type="submit" id="submitBtn">
    			        	Submit
    			        </Button>
    			        :
    			        <Button variant="primary" type="submit" id="submitBtn" disabled>
    			        	Submit
    			        </Button>
    			   }
    		    </Form>

        </Col>
      </Row>

    		</Container>

    		    
   		)
}

