import {Form, Button, Container, Row, Col} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {Navigate, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register(){
	const {user} = useContext(UserContext)

	const navigate = useNavigate()

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [password2, setPassword2] = useState('')

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setmobileNo] = useState('')

	//for determining if button is disabled or not
	const [isActive, setIsActive] = useState(false)

	function registerUser(event){
		event.preventDefault()


		fetch(`${process.env.REACT_APP_API_URL}/users/check-email`,{
					
				method:'POST',
					headers: {
						'Content-Type':'application/json'
					},
					body: JSON.stringify({
						email:email
					})
			})
			.then(response => response.json())
			.then(result => {
					if(result === true){
						Swal.fire({
								title: 'Oops!',
								icon:'error',
								text:'Email is already in use!'
							})
					}
					else{
						//Register request

						//Activity 
						fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
						
						method:'POST',
						headers: {
							'Content-Type':'application/json'
						},
						body: JSON.stringify({
							firstName: firstName, //value - states
							lastName: lastName,
							email:email,
							mobileNo:mobileNo,
							password:password
						})
				})

						.then(response => response.json())
						.then(result => {
							if (result !== false){
							//clear out the input fields after a form submission

								setEmail('')
								setPassword('')
								setPassword2('')
								setFirstName('')
								setLastName('')
								setmobileNo('')

								Swal.fire({
									title: "Thank you for registering!",
									icon: "success",
									text: "You have successfuly registered"
								})

								
								navigate('/login')
								
							}

							else{

								Swal.fire({
									title: "Something went wrong!!",
									icon: "error",
									text: "Please try again :("
								})
							}
						})
	
					}
					//end of else second statement	
					//Activity		

					})


						
		
	}



 useEffect(() => {
   
    if ((email !== '' && firstName !== '' && lastName !== '' && mobileNo.length === 11 && password !== '' && password2 !== '') && (password === password2)){
    	//Enables the submit button if the form data has been verified
    	setIsActive(true)	
    }else {
    	setIsActive(false)
    }


  }, [email, password, password2, firstName, lastName, mobileNo])



	return(

		(user.id !== null) ?
		<Navigate to="/products"/>
		:
		<Container>

		<Row>
        <Col></Col>
        <Col lg={6} className="mt-5">
        	<Form onSubmit = {event => registerUser(event)}className ="registerForm mt-5">
        	<h1>Sign up</h1>
		
		<Form.Group className="mb-3" controlId="firstName">
    		        <Form.Label>First Name</Form.Label>
    		        <Form.Control 
    		        type="text" 
    		        placeholder="Enter Firstname"
    		        value={firstName}
    				onChange={event => setFirstName(event.target.value)}
    				required
    				 />
    
    		      </Form.Group>

    		     
    		     <Form.Group className="mb-3" controlId="lastName">
    		        <Form.Label>Last Name</Form.Label>
    		        <Form.Control 
    		        type="text" 
    		        placeholder="Enter Lastname"
    		        value={lastName}
    				onChange={event => setLastName(event.target.value)}
    				required
    				 />
    
    		      </Form.Group>


    		     
    		     <Form.Group className="mb-3" controlId="mobileNo">
    		        <Form.Label>Mobile Number</Form.Label>
    		        <Form.Control 
    		        type="text" 
    		        placeholder="Enter Mobile Number"
    		        value={mobileNo}
    				onChange={event => setmobileNo(event.target.value)}
    				required
    				 />
    
    		      </Form.Group>
	        <Form.Group controlId="userEmail">
	            <Form.Label>Email address</Form.Label>
	            <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                value={email}
	                onChange={event => setEmail(event.target.value)}
	                required
	            />
	            <Form.Text className="text-muted">
	                We'll never share your email with anyone else.
	            </Form.Text>
	        </Form.Group>

	        <Form.Group controlId="password">
	            <Form.Label>Password</Form.Label>
	            <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                value={password}
	                //set changes for the email value
	                onChange={event => setPassword(event.target.value)}
	                required
	            />
	        </Form.Group>

	        <Form.Group controlId="password2">
	            <Form.Label>Verify Password</Form.Label>
	            <Form.Control 
	                type="password" 
	                placeholder="Verify Password" 
	                value={password2}
	                onChange={event => setPassword2(event.target.value)}
	                required
	            />
	        </Form.Group>
	        {	isActive ? 
	            <Button variant="primary" type="submit" id="submitBtn">
	            	Submit
	            </Button>
	            :
	            <Button className="mt-2" variant="primary" type="submit" id="submitBtn" disabled>
	            	Submit
	            </Button>
	        }
	        
		</Form>

        </Col>
        <Col></Col>
      </Row>
		
		</Container>
		)

}

