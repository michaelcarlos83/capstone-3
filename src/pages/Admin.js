import AdminDashboard from '../components/AdminDashboard'
import {useEffect, useState} from 'react'
import Loading from '../components/Loading'

// import courses_data from '../data/courses' mock data No longer needed

export default function Products(){
//.map creates a new array to iterate through the data 
	const[products, setProducts] = useState([])
	const[isLoading, setIsLoading] = useState(false)

	useEffect((isLoading) => {
		//sets the loading state to true
		setIsLoading(true)
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(response => response.json())
		.then(result => {
			setProducts(
				result.map(product => {
							return (
								
						<AdminDashboard key={product._id} product={product}/>
							
						)
					})
			       )
			//Sets the loading state to false
			setIsLoading(false)

 	        })
    }, [])

			return(
				
					(isLoading === true) ?

						<Loading/>
						
						:

						<>
						{products}		
						</>				
			)
		}
