import {Button, Row, Col} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom'
import {useParams, useNavigate} from 'react-router-dom'


export default function Banner(){

const navigate = useNavigate()


	return (
		<Row>
			<Col className="p-5">
				<h1>Your One Stop Tech Shop</h1>
				<p>Opportunities for everyone, everywhere!</p>
				<Button variant="primary" as={NavLink} to="/products">Browse Products</Button>
			</Col>
		</Row>
	)
}