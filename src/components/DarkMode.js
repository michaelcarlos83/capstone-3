import React, { useState, useEffect } from 'react';

function DarkMode() {
  const [theme, setTheme] = useState('light');
  const toggleTheme = () => {
    if (theme === 'light') {
      setTheme('dark');
    } else {
      setTheme('light');
    }
  };
  useEffect(() => {
    document.body.className = theme;
  }, [theme]);
  return (
      <button className={`DarkMode rounded ${theme}`} onClick={toggleTheme}>Dark Mode</button>
  );
}
export default DarkMode;