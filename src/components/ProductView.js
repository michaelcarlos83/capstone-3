import {useState, useEffect, useContext} from 'react'
import {Container, Card, Button, Row, Col} from 'react-bootstrap'
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function ProductView() {

		const {productId} = useParams()

		const {user} = useContext(UserContext)
		const navigate = useNavigate()


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const checkout = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method:'POST',
			headers:{
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				userId:user.id
			})
		})
		.then(response => response.json())
		.then(result => {
			if (result){
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "You have checkouted successfully"
				})

				navigate('/courses')
			}else{

				Swal.fire({
					title: "Something went wrong!!",
					icon: "error",
					text: "Please try again :("
				})
			}
		})
	}

	useEffect (() => {
			fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
			.then(response => response.json())
			.then(result => {
				setName(result.name)
				setDescription(result.description)
				setPrice(result.price)
			})
	},[productId])

		{return(
		<Container className="mt-5">
		<Row>
		<Col lg={{ span: 6, offset: 3 }}>
		<Card className="bg-secondary text-light">
		<Card.Body className="text-center">
			<Card.Title>{name}</Card.Title>
			<Card.Subtitle>Description:</Card.Subtitle>
			<Card.Text>{description}</Card.Text>
			<Card.Subtitle>Price:</Card.Subtitle>
			<Card.Text>PhP {price}</Card.Text>
			{ (user.id !== null) ?
				<Button variant="primary" onClick={() => checkout(productId)} block>Checkout</Button>

				:

				<Link className="btn btn-danger btn-block" to="/login">Log in to Purchase</Link>

			}
			
		</Card.Body>		
		</Card>
		</Col>
		</Row>
		</Container>
		)
		}}