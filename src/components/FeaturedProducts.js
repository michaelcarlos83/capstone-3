import Carousel from 'react-bootstrap/Carousel';

export default function FeaturedProducts() {
  return (
    <Carousel className="my-5">
      <Carousel.Item>
        <img
          className="d-flex w-5"
          src="https://images.indianexpress.com/2022/08/DJI-Avata.jpg" width="1300" height="700"
          alt="First slide"
        />
        <Carousel.Caption>
          <h3>DJI Avata</h3>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-flex w-5"
          src="https://thesmartlocal.com/philippines/wp-content/uploads/2022/02/Mechanical-Keyboard_Cheapest-Keyboard.jpg" width="1300" height="700"
          alt="Second slide"
        />

        <Carousel.Caption>
          <h3>Mechanical Keyboard</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-flex w-100"
          src="https://cdne-marshall-assets.azureedge.net/content/uploads/c157a1bc-6597-4552-8b64-08c3132e3c34.jpg?20190509075035"
          alt="Third slide" height="700"
        />

        <Carousel.Caption>
          <h3>Marshall Speakers</h3>
          <p>
            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

