import {Row, Col, Card, Container} from 'react-bootstrap'
import '../App.css'

export default function Highlights(){
return(
    
<Row id="Highlights" className="mt-3 mb-3 text-light">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-1 bg-dark">
                    <Card.Body>
                        <Card.Img variant="top" src="https://www.easa.europa.eu/sites/default/files/inline-images/Drones-strategy-2.0-FEATURED-image-LIGHT_0.png" className="img-fluid"/>
                        <Card.Title>
                            <h2>Drones</h2>
                        </Card.Title>
                        <Card.Text>
                            Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit irure mollit officia incididunt ea ullamco laboris excepteur amet. Cillum pariatur consequat adipisicing aute ex.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-1 bg-dark">
                    <Card.Body>
                    <Card.Img variant="top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTE4btZ9pQ-5EO5F7qUkS4h_BKVOaBBpFcNBSAUr4A1Pu54bPHuVEEmXoeqh0vK4oGu2FI&usqp=CAU" className="img-fluid"/>
                        <Card.Title>
                            <h2>Computer Accessories</h2>
                        </Card.Title>
                        <Card.Text>
                            Ex Lorem cillum consequat ad. Consectetur enim sunt amet sit nulla dolor exercitation est pariatur aliquip minim. Commodo velit est in id anim deserunt ullamco sint aute amet. Adipisicing est Lorem aliquip anim occaecat consequat in magna nisi occaecat consequat et. Reprehenderit elit dolore sunt labore qui.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-1 bg-dark">
                    <Card.Body>
                     <Card.Img variant="top" src="https://canada.crutchfieldonline.com/ImageBank/v20210226083100/core/learn/article/4236/Wall-of-speakers-1200x800.jpg" className="img-fluid"/>
                        <Card.Title>
                            <h2>Speakers</h2>
                        </Card.Title>
                        <Card.Text>
                            Minim nostrud dolore consequat ullamco minim aliqua tempor velit amet. Officia occaecat non cillum sit incididunt id pariatur. Mollit tempor laboris commodo anim mollit magna ea reprehenderit fugiat et reprehenderit tempor. Qui ea Lorem dolor in ad nisi anim. Culpa adipisicing enim et officia exercitation adipisicing.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>

)
} 