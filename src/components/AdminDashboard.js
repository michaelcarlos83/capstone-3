import {useState, useEffect, useContext} from 'react'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';   
import PropTypes from 'prop-types'
import {Link, useNavigate, useParams} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'





export default function AdminDashboard({product}) {

	const navigate = useNavigate()
	const {productId} = useParams()


  const {name, description, price, _id, isActive} = product
  const {user} = useContext(UserContext)



const remove = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/productId/archive`, {
			method:'PATCH',
			headers:{
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive:false,
			})
		})
		.then(response => response.json())
		.then(result => {
			if (result){
				Swal.fire({
					title: "",
					icon: "success",
					text: "You have deleted the item successfully"
				})

				navigate('/products')
			}else{

				Swal.fire({
					title: "Something went wrong!!",
					icon: "error",
					text: "Please try again :("
				})
			}
		})
	}


  
  return (
  	 	
    <Card className="bg-secondary my-3" style={{ width: '50rem' }}>
      <Card.Img variant="top"/>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>
          <strong>Description:</strong>
        </Card.Subtitle>
        <Card.Text>
          {description}
        </Card.Text>
        <Card.Subtitle>
          <strong>Price:</strong>
        </Card.Subtitle>
        <Card.Text>
          <strong>PHP {price}</strong>
        </Card.Text>
        <Link className="btn btn-primary" to={`/products/${_id}`}>Update</Link>
       <Link className="btn btn-danger" onClick={() => remove(productId)}>Delete</Link>
      </Card.Body>
    </Card>

  );
}