import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

	//gets the course if from the url of the route that this component is connected to'/courses/:productId'
export default function UpdateAdmin() {

		const {productId} = useParams()

		const {user} = useContext(UserContext)
		const navigate = useNavigate()


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	


		{return(
		<Container className="mt-5">
		<Row>
		<Col lg={{ span: 6, offset: 3 }}>
		<Card>
		<Card.Body className="text-center">
			<Card.Title>{name}</Card.Title>
			<Card.Subtitle>Description:</Card.Subtitle>
			<Card.Text>{description}</Card.Text>
			<Card.Subtitle>Price:</Card.Subtitle>
			<Card.Text>PHP {price}</Card.Text>
			{ (user.id !== null) ?
				<Button variant="primary"block>Save</Button>

				:

				<Link className="btn btn-danger btn-block" to="/login">Cancel</Link>

			}
			
		</Card.Body>		
		</Card>
		</Col>
		</Row>
		</Container>
		)
		}}