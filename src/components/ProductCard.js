import {useState, useEffect} from 'react'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';   
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'
import AOS from 'aos'
import 'aos/dist/aos.css'


export default function ProductCard({product}) {

  const {name, description, price, _id} = product


  return (
    <div id="productCards">
    <Card className="m-3 shadow-lg text-light"  style={{ width: '25rem' }}>
      <Card.Img variant="top" src=""/>
      <Card.Body className="cardCourse bg-secondary rounded">
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>
          <strong>Description:</strong>
        </Card.Subtitle>
        <Card.Text>
          {description}
        </Card.Text>
        <Card.Subtitle>
          <strong>Price:</strong>
        </Card.Subtitle>
        <Card.Text>
          <strong>PHP {price}</strong>
        </Card.Text>
        <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
      </Card.Body>
    </Card>
    </div>

  );
}



