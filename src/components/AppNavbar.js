import {Navbar, Nav, NavDropdown, Container} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import '../App.css';  
import DarkMode from '../components/DarkMode'



export default function AppNavbar(){
	const {user} = useContext(UserContext)




	return (
		<Navbar className="navBar" collapseOnSelect expand="lg" bg="dark" variant="dark">
		     <Navbar.Brand as={Link} to="/" className="navLogo mx-5">MCarlos</Navbar.Brand>
		     <Container>
		       
		       <Navbar.Toggle aria-controls="responsive-navbar-nav" />
		       <Navbar.Collapse id="responsive-navbar-nav">
		         <Nav className="me-auto">
		           <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
		           <Nav.Link as={NavLink} to="">Pricing</Nav.Link>
		           
		                      <DarkMode/>
		         </Nav>
		         <Nav>

		         {!user.id && (
		         	<>
		        <Nav.Link as={NavLink} to="/login"> Login </Nav.Link>
		      	<Nav.Link as={NavLink} to="/register"> Register</Nav.Link>	
		      	</>
		         )}
		         {user.id && !user.isAdmin &&(
		         	<Nav.Link as={NavLink} to="/logout">logout</Nav.Link>
		         )}

		         {user.isAdmin && (
		         	  <>
		     	<NavDropdown className="text-decoration-none" title="Admin Dashboard" id="collasible-nav-dropdown">
		             <NavDropdown.Item as={NavLink} to="/admin-dashboard">Edit Products</NavDropdown.Item>
		             <NavDropdown.Item href="#action/3.2">
		               Edit Users
		             </NavDropdown.Item>
		             <NavDropdown.Item href="#action/3.3">View Orders</NavDropdown.Item>
		             <NavDropdown.Divider />
		             <NavDropdown.Item as={NavLink} to="/logout">
		               Logout
		             </NavDropdown.Item>
		           </NavDropdown>
		       	</>

		         )}
		         </Nav>
		       </Navbar.Collapse>
		     </Container>
		   </Navbar>
	)
}