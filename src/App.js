import './App.css';  
import {useState} from 'react'
import {UserProvider} from './UserContext'
import AppNavbar from './components/AppNavbar'
import Home from './pages/Home'
import Products from './pages/Products'
import ProductView from './components/ProductView'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Admin from './pages/Admin'
import ErrorPage from './pages/ErrorPage'
import {Container} from 'react-bootstrap'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'



function App() {
// document.body.style = 'background: red;';

  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })


    const unsetUser = () => {
      localStorage.clear()
    }

  return (
    <>

  {/*Provides the user context throughout any component inside of it*/}


      <UserProvider value={{user, setUser, unsetUser}}>
         {/*Initializes that dynamic routing will be involved*/}
    <Router>
    <AppNavbar/>
       <Container>       
      <Routes>
          <Route path="/" element={<Home/>}/>
          <Route path="/products" element={<Products/>}/>
          <Route path="/products/:productId" element={<ProductView/>}/>
          <Route path="/login" element={<Login/>}/>
          <Route path="/register" element={<Register/>}/>
          <Route path="/logout" element={<Logout/>}/>
          <Route path="*" element={<ErrorPage/>}/>         
          <Route path="/admin-dashboard" element={<Admin/>}/>         
        </Routes>
       </Container>
    </Router>
      </UserProvider>
    </> 
  );
}

export default App;
